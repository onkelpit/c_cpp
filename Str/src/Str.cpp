#include "../include/Str.h"

#define BUFFER_SIZE 512

Str::Str()
{
    //ctor
    this->l = 0;
    s = (char*)malloc(l*sizeof(char*));
}

Str::~Str()
{
    //dtor
    free(this->s);
}

Str::Str(const Str& other)
{
    //copy ctor
    l = other.l;
    s = (char*)malloc(l*sizeof(char*));
    strcpy(s, other.s);
}

Str::Str(const char* c)
{
    this->l = strlen(c);
    this->s = (char*)malloc(l*sizeof(char));
    strcpy(s,c);
}

char const* Str::c_str() const
{
    return(this->s);
}

int Str::len()
{
    return(l);
}

std::ostream& operator<<(std::ostream &os, const Str& org)
{
    os << org.c_str();
    return(os);
}

std::istream& operator>>(std::istream &is, Str& org)
{
    int n = 0;
    free(org.s);
    org.l = 0;
    org.s = (char*)malloc(BUFFER_SIZE*sizeof(char));
    int written_chars = 0;
    bool found_r = false;
    char c = is.peek();
    do
    {
        c = is.peek();
        if(c == '\r')
            found_r = true;
        else
            found_r = false;
        if(written_chars==(BUFFER_SIZE-1))
        {
            char* tmp = (char*)malloc(sizeof(char)*sizeof(org.s));
            free(org.s);
            org.s = (char*)malloc(sizeof(char)*(sizeof(org.s)+written_chars));
            strcpy(org.s, tmp);
            free(tmp);
        }
        is.get(org.s[n]);
        n++;
        written_chars++;
    } while(c != '\n' && c != '\0' && (!found_r && c != '\n'));
    org.l = n;
    return(is);
}

bool Str::operator==(const Str &other)
{
    if(l == other.l && strcmp(s, other.s) == 0)
        return true;
    else
        return false;
}

bool Str::operator==(const char* other)
{
    int l_other = strlen(other);
    if(l == l_other && strcmp(s, other) == 0)
        return true;
    else
        return false;
}

Str& Str::operator=(const Str &other)
{
    l = other.l;
    s = (char*)malloc(l*sizeof(char));
    strcpy(s, other.s);
    return *this;
}

Str& Str::operator=(const char* other)
{
    l = strlen(other);
    s = (char*)malloc(l*sizeof(char));
    strcpy(s, other);
    return *this;
}

Str Str::operator+(const Str &other)
{
    return(Str(strcat(s, other.s)));
}

Str Str::operator+(const char* other)
{
    return(Str(strcat(s, other)));
}
