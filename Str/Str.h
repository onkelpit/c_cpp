#ifndef STR_H
#define STR_H

#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <exception>
#include <typeinfo>

using namespace std;

class Str
{
    public:
        /** Default constructor */
        Str();
        /** Default destructor */
        virtual ~Str();
        /** Copy constructor
         *  \param other Object to copy from
         */
        Str(const Str& other);

        Str(const char*);

        char const* c_str() const;

        int len() const;

        friend std::ostream& operator<<(std::ostream &os, const Str& org);
        friend std::istream& operator>>(std::istream &is, Str& org);
        bool operator==(const Str &other);
        bool operator==(const char* other);
        Str& operator=(const Str &other);
        Str& operator=(const char* other);
        virtual void f1();
        virtual void f2();
    protected:
        char* s; //!< Member variable "s;"
        int l; //!< Member variable "l;"
};
Str operator+(const Str &left, const Str &right);
Str operator+(const Str& left, const char* right);
#endif // STR_H
