#include <math.h>
#include <exception>
#include "Str.h"
#include "WStr.h"

using namespace std;

template <class T>
T operator+(T const& s, T const& t) {
    T temp(s);
    return temp += t;
}

// GC

template <class T>
struct A {
    int* __count;

    A() {
        s = new T();
        __count = new int;
        *__count = 1;
    }

    A(T* t) {
        s = t;
        __count = new int;
        *__count = 1;
    }

    ~A() {
        if (--*__count == 0) {
            delete s;
            delete __count;
        } else {
            cout << "Element noch blockiert" << endl;
        }
    }

    A(A const& a) {
        s = a.s;
        __count = a.__count;
        *__count = *__count + 1;
    }

    A& operator=(T* t) {
        s = new T(t);
        return *this;
    }

    A& operator=(A const& a) {
        s = a.s;
        return *this;
    }

    T* operator->() {
        return s;
    }
    T const* operator->() const {
        return s;
    }

    T& operator*() {
        return *s;
    }

    T const& operator*() const {
        return *s;
    }
    T* s;
};

void f(A<Str>& a) {
    cout << a->c_str() << endl;
}

void f(A<WStr>& a) {
    cout << a->c_str() << endl;
}

int main() {
    /*
    A<WStr> b(new WStr("Test"));
    
    A<Str> a(new Str("hallo"));
    f(a);
    f(b);
    A<Str> c(a);
    cout << b->c_str() << endl;
    a->operator =("123");
    cout << c->c_str() << endl;
    cout << a->c_str() << endl;

    
    
    A<Str> aa(new WStr("Test"));
    
    
    cout << aa->c_str() << endl;
    
    aa->f2();
    aa->Str::f2();
    
    aa->Str::~Str();
    
    cout << "c_str(): " << aa->c_str() << endl;
     */

    A<Str>* a = new A<Str>(new Str("Test"));

    A<Str> b(*a);

    f(*a);
    f(b);

    delete a;

    f(b);
    return 0;
}
