/* 
 * File:   wStr.h
 * Author: pit
 *
 * Created on 14. Mai 2014, 13:57
 */

#include <iostream>
#include <typeinfo>
#include "Str.h"
#ifndef WSTR_H
#define	WSTR_H

class WStr : public Str
{
public:
    WStr();
    
    WStr(const char* constcharstern) : Str(constcharstern){
        cout << "Ctor WStr" << endl;
    };
    
    ~WStr();
    
    void f2();
};

#endif	/* WSTR_H */

