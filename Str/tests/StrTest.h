/*
 * File:   StrTest.h
 * Author: pit
 *
 * Created on 16.04.2014, 23:32:19
 */

#ifndef STRTEST_H
#define	STRTEST_H

#include <cppunit/extensions/HelperMacros.h>

class StrTest : public CPPUNIT_NS::TestFixture {
    CPPUNIT_TEST_SUITE(StrTest);

    CPPUNIT_TEST(testMethod);
    CPPUNIT_TEST(testFailedMethod);

    CPPUNIT_TEST_SUITE_END();

public:
    StrTest();
    virtual ~StrTest();
    void setUp();
    void tearDown();

private:
    void testMethod();
    void testFailedMethod();
};

#endif	/* STRTEST_H */

