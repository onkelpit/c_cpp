#include "Str.h"

#define BUFFER_SIZE 4096

/**
 * @brief Constructor
 * @param none
 */
Str::Str() {
    //ctor
    cout << "Ctor Str" << endl;
    this->l = 0;
    s = (char*) malloc(l * sizeof (char));
}

/**
 * @brief Destructor
 * @param none
 */
Str::~Str() {
    //dtor
    cout << "Dtor Str" << endl;
    if (s != NULL) {
        free(s);
        s = NULL;
    }
    else
        cout << "Element noch belegt" << endl;
}

/**
 * @brief Copy-Constructor
 * @param other Element of Str to copy
 */
Str::Str(const Str& other) {
    //copy ctor
    cout << "copy Ctor Str" << endl;
    l = other.l;
    s = (char*) malloc(l * sizeof (char));
    strcpy(s, other.s);
}

/**
 * @brief Construcor
 * @param c char* as String for the new object
 */
Str::Str(const char* c) {
    cout << "Ctor Str" << endl;
    this->l = strlen(c);
    this->s = (char*) malloc(l * sizeof (char));
    strcpy(s, c);
}

/**
 * @brief c_str();
 * @return returns a pointer to an char-array
 */
char const* Str::c_str() const {
    return (this->s);
}

/**
 * \brief length
 * \return returns the length of the String
 */
int Str::len() const {
    return (l);
}

std::ostream& operator<<(std::ostream &os, const Str& org) {
    os << org.c_str();
    return (os);
}

std::istream& operator>>(std::istream &is, Str& org) {
    try {
        int n = 0;
        free(org.s);
        org.l = 0;
        char* buffer = (char*) malloc(BUFFER_SIZE * sizeof (char));
        int written_chars = 1;
        bool found_r = false;
        char c = is.peek();
        while (c != '\n' && c != '\0' && (!found_r && c != '\n')) {
            c = is.peek();
            if (c == '\r')
                found_r = true;
            else
                found_r = false;
            if (c != '\n') {
                if (written_chars == (BUFFER_SIZE - 1)) {
                    int size_buffer = strlen(buffer) + 1;
                    char* tmp = (char*) malloc(sizeof (char)*size_buffer);
                    strcpy(tmp, buffer);
                    free(buffer);
                    buffer = (char*) malloc(sizeof (char)*(size_buffer + written_chars));
                    strcpy(buffer, tmp);
                    free(tmp);
                    written_chars = 0;
                }
                is.get(buffer[n]);
                n++;
                written_chars++;
            }
        }
        org.l = n;
        org.s = (char*) malloc((n + 1) * sizeof (char));
        strcpy(org.s, buffer);
    } catch (exception& e) {
        cout << "Exception caught: " << e.what() << endl;
    }

    return (is);
}

bool Str::operator==(const Str &other) {
    if (l == other.l && strcmp(s, other.s) == 0)
        return true;
    else
        return false;
}

bool Str::operator==(const char* other) {
    int l_other = strlen(other);
    if (l == l_other && strcmp(s, other) == 0)
        return true;
    else
        return false;
}

Str& Str::operator=(const Str &other) {
    l = other.l;
    s = (char*) malloc(l * sizeof (char));
    strcpy(s, other.s);
    return *this;
}

Str& Str::operator=(const char* other) {
    l = strlen(other);
    s = (char*) malloc(l * sizeof (char));
    strcpy(s, other);
    return *this;
}

void Str::f1() {
    cout << "Str f1()" << endl;
}

void Str::f2() {
    cout << "Str f2()" << endl;
}

Str operator+(const Str &left, const Str& right) {
    int _l = left.len() + right.len();
    char* buffer = (char*) malloc(_l * sizeof (char));
    strcpy(buffer, left.c_str());
    char* part_buffer = buffer + left.len();
    strcpy(part_buffer, right.c_str());

    Str tmp(buffer);
    return (tmp);

}

Str operator+(const Str& left, const char* right) {
    int _l = left.len() + strlen(right);
    char* buffer = (char*) malloc(_l * sizeof (char));
    strcpy(buffer, left.c_str());
    char* part_buffer = buffer + left.len();
    strcpy(part_buffer, right);

    Str tmp(buffer);
    return (tmp);
}

