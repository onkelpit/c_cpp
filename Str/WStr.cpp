/* 
 * File:   WStr.cpp
 * Author: pit
 * 
 * Created on 14. Mai 2014, 13:57
 */

#include "WStr.h"

WStr::WStr() {
    std::cout << "Ctor WStr" << std::endl;
}

WStr::~WStr() {
    std::cout << "Dtor WStr" << std::endl;
    if (Str::s != NULL) {
        free(Str::s);
        Str::s = NULL;
    }
}

void WStr::f2() {
    std::cout << "WStr f2()" << std::endl;
}
