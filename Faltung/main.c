#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define BOOL    int
#define NUMBER_OF_G 3

#define PI 3.14159265

typedef double (*func) (double);

func MULT_FUNC[2];
double OFFSET;

double calcSimpson(double a, double b, func fu);
double calcMinimalStepSize(double lBound, double rBound, func fu);
int calcIntegralProd(double a, double b, int c, func f, func *g, double *ret);
double f(double x);
double h(double x);

double* _faltung(double lBound, double rBound, int c, func f, func g);

double multFunc(double x)
{
    return((double)MULT_FUNC[0](x)*MULT_FUNC[1](x-OFFSET));
}

double* faltung(double a, double b, size_t npoints, func f, func g)
{
    double* res=(double*)malloc(npoints*sizeof(double));
    MULT_FUNC[0]=f;
    MULT_FUNC[1]=g;
    size_t i;
    for(i=0; i<npoints; i++)
    {
        OFFSET=a+i*(b-a)/npoints;
        res[i]=calcSimpson(a,b,&multFunc);
    }
    return res;
}


int main()
{
    double piN = calcSimpson(-2.0, 2.0, f);
    printf("PiN: %.16f\n", piN);
    func* g = (func*)malloc(NUMBER_OF_G*sizeof(func));
    int i = 0;

    g[0] = &cos;
    g[1] = &sin;
    g[2] = &h;/*
    g[3] = &tan;
    g[4] = &atan;
    g[5] = &acos;
    g[6] = &asin;*/

    double *r = (double*)malloc(NUMBER_OF_G*sizeof(double));
    /*for(i = 0; i < NUMBER_OF_G; i++)
        r[i] = i;*/
    calcIntegralProd(-2.0, 2.0, NUMBER_OF_G, &f, g, r);

    for(i = 0; i < NUMBER_OF_G; i++)
        printf("Prod_%i:\t%.16lf\n",i, r[i]);


    double* res = faltung(0, 3, 20, &sin, &cos);
    for(i = 0; i < 20; i++)
        printf("%lf ", res[i]);


    printf("\n");
    free(res);
    free(g);
    return 0;
}

double calcSimpson(double lBound, double rBound, func fu)
{
    double stepSize = calcMinimalStepSize(lBound, rBound, fu);
    double ret = 0.0;
    BOOL squared = 0;
    double n = 0.0;
    for(n = lBound; n <= rBound; n += stepSize)
    {
        if(n==lBound || n>=rBound)
            ret += fabs(fu(n)*stepSize);
        else
        {
            ret += ((squared == 1) ? 2 : 4) * fabs(fu(n)*stepSize);
            squared = (squared == 1) ? 0 : 1;
        }
    }
    ret /= 3.0;
    return(ret);
}

double calcMinimalStepSize(double a, double b, func fu)
{
    double stepSize = 1e-6;
    double x0 = fu(a);
    double x1;
    do
    {
        x1 = fu(stepSize);
        stepSize /= 2.0;
    }
    while (fabs(x0)-fabs(x1)>1e9);
    return stepSize;
}

int calcIntegralProd(double a, double b, int c, func f, func *g, double *ret)
{
    int i = 0;
    for(i = 0; i < c; i++)
    {
        double stepSizef, stepSizeg = 0.0;
        stepSizef = calcMinimalStepSize(a, b, f);
        stepSizeg = calcMinimalStepSize(a, b, g[i]);

        double stepSize = (stepSizef<stepSizeg) ? stepSizef : stepSizeg;
        double erg = 0.0;
        BOOL squared = 0;
        double n = 0.0;
        for(n = a; n <= b; n += stepSize)
        {
            if(n==a || n>=b)
            {
                erg += ((g[i](n))*(f(n))*stepSize);
            }
            else
            {
                erg += ((squared == 1) ? 2 : 4) * ((g[i](n))*(f(n))*stepSize);
                squared = (squared == 1) ? 0 : 1;
            }
        }
        erg /= 3.0;
        ret[i] = erg;
    }
    return c;
}

double f(double x)
{
    return(sqrt(1-pow((fabs(x)-1),2)));
}

double h(double x)
{
    return(pow(x,2));
}
