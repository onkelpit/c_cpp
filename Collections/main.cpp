/* 
 * File:   main.cpp
 * Author: pit
 *
 * Created on 15. Mai 2014, 08:57
 */

#include <iostream>
#include <cstdlib>
#include <vector>
#include <deque>
#include <string>
#include <list>

using namespace std;

template<typename T>
class dvl
{
public:
    dvl();
    virtual ~dvl();
    
    T* begin();
    T* end;
    bool has_next();
    
    
private:
    T* prev;
    T* next;
};

/*
 * 
 */
int main(int argc, char** argv) {
    vector<int> v;
    v.resize(5);
    v[2]=3;
    v.at(4)=7;
    cout << v.size() << endl;
    v.push_back(42);
    cout << v.size() << endl;

    deque<string> s;
    
    for (int i = 0; i < 10; i++) {
        s.push_back("Test");
    }
    
    
    for(string _s : s)
    {
        cout << _s << endl;
    }
    
    list<int> l;
    
    
    for (int i = 0; i < 3; i++) {
        l.push_back(42);
    }

    
    for(auto el : l)
        cout << el << endl;
    
    return 0;
}

